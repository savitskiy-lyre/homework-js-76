import React, {useEffect} from "react";
import Layout from "./components/UI/Layout/Layout";
import './App.css';
import {Box, Grid} from "@mui/material";
import {useDispatch, useSelector} from "react-redux";
import {fetchPosts, postPost} from "./store/actions/postsActions";
import Post from "./components/Post/Post";
import FormPost from "./components/FormPost/FormPost";

const App = () => {
   const dispatch = useDispatch();
   const sendBtnLoading = useSelector(state => state.posts.sendBtnLoading);
   const errorForm = useSelector(state => state.posts.formError);
   const posts = useSelector(state => state.posts.posts);

   useEffect(() => {
      if (posts !== null) {
         const requestIntervalPosts = () => {
            dispatch(fetchPosts(posts[posts.length - 1]['datetime']));
         }
         const interval = setInterval(requestIntervalPosts, 3000);
         return () => {
            clearInterval(interval);
         }
      } else {
         dispatch(fetchPosts());
      }
   }, [posts, dispatch]);

   return (
     <Layout>
        <Box py={2}>
           <FormPost
             sendBtnLoading={sendBtnLoading}
             onSubmitHandler={(data) => dispatch(postPost(data))}
             errorForm={errorForm}
           />
           <Grid container textAlign={"center"}>
              {posts ? posts.map((post) => (
                <Post
                  author={post.author}
                  message={post.message}
                  datetime={post.datetime}
                  key={post.id}
                />
              )).reverse() : (
                <div className="posts-preloader"/>
              )}
           </Grid>
        </Box>
     < /Layout>
   )
     ;
};

export default App;
