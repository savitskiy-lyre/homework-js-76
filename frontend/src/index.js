import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import {BrowserRouter} from "react-router-dom";
import {applyMiddleware, combineReducers, compose, createStore} from "redux";
import {Provider} from "react-redux";
import thunk from "redux-thunk";
import {postsReducer} from "./store/reducers/postsReducer";

const rootReducer = combineReducers({
   posts: postsReducer,
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(rootReducer, composeEnhancers(
  applyMiddleware(thunk)
))

ReactDOM.render(
  <BrowserRouter>
     <Provider store={store}>
        <App/>
     </Provider>
  </BrowserRouter>
  , document.getElementById('root')
);
