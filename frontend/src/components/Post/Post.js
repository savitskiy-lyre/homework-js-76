import React from 'react';
import {Divider, Grid, Stack, Typography} from "@mui/material";

const Post = ({author, message, datetime}) => {
   return (
     <Grid item xs={12} sm={6} md={4} lg={3}>
        <Stack spacing={2} sx={{border: '2px solid gainsboro', background: "white", borderRadius: '4px'}}
               m={'5px'} p={'6px'}>
           <Typography variant={'body1'} pt={1}>
              <strong>{author} said:</strong>
           </Typography>
           <Divider/>
           <Typography variant={'body1'}>{message}</Typography>
           <Typography variant={'subtitle1'}
                       sx={{fontSize: '12px', color: "gray"}}
           >
              <i>{datetime}</i>
           </Typography>
        </Stack>
     </Grid>
   );
};

export default Post;