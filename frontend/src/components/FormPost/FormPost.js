import React, {useState} from 'react';
import {Grid, TextField} from "@mui/material";
import {LoadingButton} from "@mui/lab";
import SendIcon from "@mui/icons-material/Send";

const FormPost = ({errorForm, sendBtnLoading, onSubmitHandler}) => {
   const [profile, setProfile] = useState('');
   const [postInp, setPostInp] = useState('');

   const onMessageSubmit = async (e) => {
      e.preventDefault();
      const data = {
         message: postInp,
         author: profile,
      };
      onSubmitHandler(data)
      setPostInp('');
      setProfile('');
   };
   return (
     <Grid container component={'form'} onSubmit={onMessageSubmit} mb={2} spacing={2} justifyContent={"center"}>
        <Grid item xs={12} md={6}>
           <TextField
             error={!!errorForm?.author}
             helperText={errorForm ? errorForm.author : ''}
             fullWidth
             variant={"standard"}
             label={'Author'}
             value={profile}
             onChange={(e) => setProfile(e.target.value)}
           />
        </Grid>
        <Grid item xs={12} md={6}>
           <TextField
             error={!!errorForm?.message}
             helperText={errorForm ? errorForm.message : ''}
             fullWidth
             variant={"standard"}
             label={'What\'s Happening ?'}
             value={postInp}
             onChange={(e) => setPostInp(e.target.value)}
           />
        </Grid>
        <Grid item>
           <LoadingButton
             type={"submit"}
             endIcon={<SendIcon/>}
             loading={sendBtnLoading}
             loadingPosition="end"
             variant="contained"
           >
              Send
           </LoadingButton>
        </Grid>
     </Grid>
   );
};

export default FormPost;