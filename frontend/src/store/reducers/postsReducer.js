import {
   FETCH_POSTS_FAILURE,
   FETCH_POSTS_REQUEST,
   FETCH_POSTS_SUCCESS,
   POST_POST_FAILURE, POST_POST_REQUEST, POST_POST_SUCCESS
} from "../actions/postsActions";

const initState = {
   error: null,
   formError: null,
   sendBtnLoading: false,
   posts: null,
};
export const postsReducer = (state = initState, action) => {
   switch (action.type) {
      case FETCH_POSTS_REQUEST:
         return {...state, error: null};
      case FETCH_POSTS_SUCCESS:
         return {...state, posts: action.payload};
      case FETCH_POSTS_FAILURE:
         return {...state, error: action.payload};
      case POST_POST_REQUEST:
         return {...state, sendBtnLoading: true, formError: null};
      case POST_POST_SUCCESS:
         return {...state, sendBtnLoading: false};
      case POST_POST_FAILURE:
         return {...state, sendBtnLoading: false, formError: action.payload};
      default:
         return state;
   }
}