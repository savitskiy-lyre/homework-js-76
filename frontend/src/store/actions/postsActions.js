import {MESSAGES_URL} from "../../config";
import {axiosApi} from "../../axiosApi";

export const FETCH_POSTS_REQUEST = 'FETCH_POSTS_REQUEST';
export const FETCH_POSTS_SUCCESS = 'FETCH_POSTS_SUCCESS';
export const FETCH_POSTS_FAILURE = 'FETCH_POSTS_FAILURE';
export const POST_POST_REQUEST = 'POST_POST_REQUEST';
export const POST_POST_SUCCESS = 'POST_POST_SUCCESS';
export const POST_POST_FAILURE = 'POST_POST_FAILURE';

export const fetchPostsRequest = () => ({type: FETCH_POSTS_REQUEST});
export const fetchPostsSuccess = (posts) => ({type: FETCH_POSTS_SUCCESS, payload: posts});
export const fetchPostsFailure = (err) => ({type: FETCH_POSTS_FAILURE, payload: err});

export const fetchPosts = (date) => {
   return async (dispatch, getState) => {
      try {
         dispatch(fetchPostsRequest());
         if (date) {
            const {data} = await axiosApi.get(MESSAGES_URL + '?datetime=' + date);
            if (data.length > 0){
               const posts = getState().posts.posts;
               dispatch(fetchPostsSuccess(posts.concat(data)));
            }
         } else {
            const {data} = await axiosApi.get(MESSAGES_URL);
            dispatch(fetchPostsSuccess(data));
         }
      } catch (error) {
         dispatch(fetchPostsFailure(error));
      }
   }
}

export const postPostRequest = () => ({type: POST_POST_REQUEST});
export const postPostSuccess = () => ({type: POST_POST_SUCCESS});
export const postPostFailure = (err) => ({type: POST_POST_FAILURE, payload: err});

export const postPost = (post) => {
   return async (dispatch) => {
      try {
         dispatch(postPostRequest());
         await axiosApi.post(MESSAGES_URL, post);
         dispatch(postPostSuccess());
      } catch (error) {
         dispatch(postPostFailure(error.response.data));
      }
   }
}