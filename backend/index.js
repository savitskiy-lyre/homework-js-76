const express = require("express");
const bodyParser = require('body-parser');
const cors = require('cors');
const fileDb = require('./DB/db');
const encode = require('./sections/encode');
const decode = require('./sections/decode');
const messages = require('./sections/messages');
const constants = require("./constants");

const app = express();
app.use(bodyParser.json());
app.use(cors());

const port = 8080;

app.use(constants.ENCODE_PATH, encode);
app.use(constants.DECODE_PATH, decode);
app.use(constants.MESSAGES_PATH, messages);

app.get('/:url', (req, res) => {
   res.send(req.params.url);
})
app.get('/', (req, res) => {
   res.send('Hello');
})
fileDb.init();
app.listen(port, () => {
   console.log('We are live ~~!!! ' + port);
})